@extends('template')

@section('title')
Compétences
@endsection

@section('header')
<div class="btn-group headerButtons" role="group" aria-label="Basic example">
  <a href="{{route('experiences')}}" type="button" class="btn btn-primary bleu">Expériences professionnelles</a>
  <a href="{{route('accueil')}}" type="button" class="btn btn-primary bleu">Accueil</a>
  <a href="{{route('formations')}}" type="button" class="btn btn-primary bleu">Formations et diplômes</a>
</div>
@endsection

@section('content')
<div class="container">
  <img id="logoC" src="{{asset('templates/css/images/csharplogo.png')}}" alt="Logo C#">
  <img id="logoHC" src="{{asset('templates/css/images/htmlcsslogo.png')}}" alt="Logo Html/Css">
  <img id="logoL" src="{{asset('templates/css/images/laravellogo.png')}}" alt="Logo Laravel">
  <img id="logoS" src="{{asset('templates/css/images/sqlservermanagementlogo.png')}}" alt="Logo SQL Server Managment">
</div>
@stop
