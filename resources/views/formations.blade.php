@extends('template')

@section('title')
Formations et diplômes
@endsection

@section('header')
<div class="btn-group headerButtons" role="group" aria-label="Basic example">
  <a href="{{route('experiences')}}" type="button" class="btn btn-primary bleu">Expériences professionnelles</a>
  <a href="{{route('accueil')}}" type="button" class="btn btn-primary bleu">Accueil</a>
  <a href="{{route('competences')}}" type="button" class="btn btn-primary bleu">Compétences</a>
</div>
@endsection

@section('content')
<div class="base">
  <h2>Formations</h2><br>

  <ul>
      <li>2018-2020 <br> BTS SIO (Service Informatique aux Organisations) à Pasteur Mont-Roland (39100 Dole). Option SLAM (Solutions Logicielles et Applications Métier). <br> Obtention du BTS.</li><br>
      <li>2014-2017 <br> Bac Pro SEN (Système Electronique Numérique à Pasteur Mont-Roland (39100 Dole). Option TR (Télécommunication Réseau). Obtention du baccalauréat avec mention assez bien.</li><br>
      <li>2010-2014 <br> Collège Jeanne D’Arc (01 Gex). Option Anglais-Espagnol LV1. Obtention du brevet des Collèges.</li>
  </ul>
</div>
@stop
