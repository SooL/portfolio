@extends('template')

@section('title')
Expériences professionnelles
@endsection

@section('header')
<div class="btn-group headerButtons" role="group" aria-label="Basic example">
  <a href="{{route('formations')}}" type="button" class="btn btn-primary bleu">Formations et diplômes</a>
  <a href="{{route('accueil')}}" type="button" class="btn btn-primary bleu">Accueil</a>
  <a href="{{route('competences')}}" type="button" class="btn btn-primary bleu">Compétences</a>
</div>
@endsection

@section('content')

<div class="d-inline-flex p-2 base">
  <h2>Expériences professionnelles</h2><br>
  <ul>
    <li>Hello Dole <br> Stage du 06 janvier au 14 février 2020 <br> Projet PowerShell, gestion de la photothèque de l'Office de Tourisme de Dole via un script</li><br>
    <li>Pasteur Mont-Roland <br>Projet 2ème semestre 2020 <br> Mise en place d'une solution web pour aider les entreprises à trouver des stagiaires</li><br>
    <li>CERN (Conseil Européen pour la Recherche Nucléaire) de Meyrin <br> Stage du 03 juin au 28 juin 2019 <br> Projet Javascript, développement d’un système de notifications pour le centre de contrôle du CMS (Compact Muon Solenoid)</li>

  </ul>
</div>
@stop
