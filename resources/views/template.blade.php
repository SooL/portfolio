<!DOCTYPE html>
<html>
<head> <!-- portefolio -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width"/>

<meta name="viewport" content="width=device-width, minimum-scale=0.25"/>

<title>@yield('title')</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-dark">
    <a class="navbar-brand"  href="{{route('accueil')}}"><span class="sr-only">(current)</span>
      <img src="{{asset('templates/css/images/planete.png')}}" width="70" height="70" alt="" loading="lazy">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('experiences')}}">Expériences professionnelles</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('formations')}}">Formations et diplômes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('competences')}}">Compétences</a>
      </li>
    </ul>
  </div>
  </nav>

  <!-- Page Content -->
  <div class="marge">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-left">
        @yield('content')
        </div>
      </div>
    </div>
</div>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>  <link rel="stylesheet" href="{{asset('templates/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('templates/css/style.css')}}">

</body>
<footer class="footer">
  <div class="headerButtons blanc">
  <a type="button" href="{{asset('CV-Remy-Fejoz.pdf')}}" target="_blank" class="btn btn-primary bleu">CV</a>
</div>
</footer>

</html>
