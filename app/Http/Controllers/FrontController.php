<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function experiences()
    {
      return view('experiences');
    }
    public function formations()
    {
      return view('formations');
    }
    public function competences()
    {
      return view('competences');
    }
    public function accueil()
    {
      return view('accueil');
    }
}
