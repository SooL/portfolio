<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'FrontController@accueil')->name('accueil');
Route::get('experiences', 'FrontController@experiences')->name('experiences');
Route::get('formations', 'FrontController@formations')->name('formations');
Route::get('competences', 'FrontController@competences')->name('competences');
